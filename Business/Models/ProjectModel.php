<?php

namespace Business\Models;

/**
 * Class ProjectModel
 * @package Business\Models
 * @property integer $ProjectId
 * @property string $Title
 * @property string $Subtitle
 * @property string $Description
 * @property string $Url
 * @property string $Image
 */
class ProjectModel {
	public $ProjectId;
	public $Title;
	public $Subtitle;
	public $Description;
	public $Url;
	public $Image;

	public function GetImageUrl() {
		if (!empty($this->Image)) {
			if (file_exists($this->GetImagePath($this->Image)) === true) {
				return sprintf("%sMedia/Projects/%s", CDN_URL, $this->Image);
			}
		}
		return sprintf("%s/Media/DefaultImages/project-default.jpg", CDN_URL);
	}

	public function GetImagePath($imageName) {
		return sprintf("%s/Media/Projects/%s", CDN_PATH, $imageName);
	}
}