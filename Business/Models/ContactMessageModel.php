<?php

namespace Business\Models;

/**
 * Class ContactMessageModel
 * @package Business\Models
 * @property integer $ContactMessageId
 * @property string $FirstName
 * @property string $LastName
 * @property string $Email
 * @property string $Message
 */


class ContactMessageModel {

	public $ContactMessageId;
	public $FirstName;
	public $LastName;
	public $Email;
	public $Message;

}