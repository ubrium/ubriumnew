<?php



namespace Business\Models;
use Business\Enums\RoleStatusesEnum;

/**
 * Class RoleModel
 * @package Business\Models
 * @property integer $RoleId
 * @property integer $Active
 * @property string $Caption
 */

class RoleModel
{
    public $Protected;
    public $Active;
    public $Caption;


    public function IsActive()
    {
        if ($this->Active == RoleStatusesEnum::Active) {
            return true;
        }
        return false;
    }

    public function SetActive($active)
    {
        if ($active) {
            $this->Active = RoleStatusesEnum::Active;
        } else {
            $this->Active = RoleStatusesEnum::Inactive;
        }
    }
}