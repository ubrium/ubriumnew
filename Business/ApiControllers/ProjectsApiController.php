<?php


namespace Business\ApiControllers;
use Business\Models\ProjectModel;
use Data\DataManagers\ProjectsDataManager;

class ProjectsApiController {

	/**
	 * @param $projectId
	 * @return ProjectModel
	 */

	/**
	 * @return ProjectModel[]
	 */
	public static function GetProjects() {
		return ProjectsDataManager::GetProjects();
	}


	public static function GetProject($projectId) {
		return ProjectsDataManager::GetProject($projectId);
	}

	public static function InsertProject($model) {
		return ProjectsDataManager::InsertProject($model);
	}

	public static function UpdateProject($model) {
		return ProjectsDataManager::UpdateProject($model);
	}

	public static function DeleteProject($projectId) {
		return ProjectsDataManager::DeleteProject($projectId);
	}


}