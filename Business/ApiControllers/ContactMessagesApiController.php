<?php

namespace Business\ApiControllers;


use Data\DataManagers\ContactMessagesDataManager;

class ContactMessagesApiController {

	public static function GetContactMessages(){
		return ContactMessagesDataManager::GetContactMessages();
	}

	public static function InsertMessage($model){
		return ContactMessagesDataManager::InsertMessage($model);
	}

}