<?php

namespace Business\Enums;


class LocalesEnum extends BaseEnum {

    const en = "en_US";
    const de = "de_DE";

}