<?php

class HtmlHelper
{

    public static function ActiveButton($controller, $action = false, $class = 'active')
    {
        global $router;
        if ($router->Controller === $controller && (!$action || $action === $router->Action)) {
            echo $class;
        }
    }

    /**
     * Creates input with autocomplete search functionality
     * @param array $options A list of options to match search query.
     * @param string $name
     * @param int $number Number of suggestions to show
     * @param string $value
     * @param string $placeholder (null) value label
     * @param array $attributes A list of attributes. Key is used as attribute name, value is used as attribute value
     * @return string
     */
    public static function AutoComplete($options, $name, $value = null, $number = null, $placeholder = null, $attributes = [])
    {
        if (is_null($placeholder)) {
            $placeholder = "placeholder";
        }

        if (is_null($name)) {
            $placeholder = "";
        }
        if ($number == null) {
            $number = 4;
        }
        if ($value = null) {
            $value = "";
        }

        // data-source='["Serbia","Belgrade","Nis"]
        /*
          <input class=" tt-input span3" type="text" data-source='["Serbia","Belgrade","Nis"]'
                 data-provide="typeahead"
                 data-items="3" autocomplete="off"/>
      </div>
      */
        $dataSource = "data-source='[";

        foreach ($options as $index => $option) {
            $optionString = sprintf('"%s"', $option);
            $dataSource .= $optionString;
            if ($index < count($options)) {
                $dataSource .= ",";
            } else {
                $dataSource .= "]'";
            }
        }

        $autoComplete = sprintf("<input name='%s' class = 'form-control' %s value='%s' data-items='%s' data-provide='typeahead' autocomplete='off' placeholder = '%s'", $name, $dataSource, $value, $number, $placeholder);
        if (count($attributes) > 0) {
            foreach ($attributes as $key => $attribute) {
                $autoComplete .= sprintf('%s = "%s" ', $key, $attribute);
            }
        }
        $autoComplete .= "/>";

        echo $autoComplete;

    }

    /**
     * @param array $options A list of options. Key is used as value attribute, value is used as label
     * @param string $name
     * @param mixed|array $selectedValue
     * @param string $label Default (null) value label
     * @param array $attributes A list of attributes. Key is used as attribute name, value is used as attribute value
     * @param string $nullValue
     * @param bool $multiselect
     * @return string
     */
    public static function SearchableSelect($options, $name, $selectedValue = null, $label = null, $attributes = [], $nullValue = "", $multiselect = false)
    {
        $multiple = "";
        $title = "";
        if (is_null($label)) {
            $label = "Select";
        }
        if (is_null($name)) {
            $name = "";
        }
        if ($multiselect) {
            if (substr($name, strlen($name) - 2, 2) !== "[]") {
                $name .= ("[]");
                $multiple = "multiple";
                $title = sprintf("title='%s'", $label);
            }
        }
        $select = sprintf("<select name='%s' class='selectpicker form-control show-tick' data-live-search='' %s %s ", $name, $multiple, $title);


        if (count($attributes) > 0) {
            foreach ($attributes as $key => $attribute) {
                $select .= sprintf("%s = '%s' ", $key, $attribute);
            }
        }
        $select .= ">";
        if (!$multiselect && !is_null($nullValue)) {
            $select .= sprintf("<option value='%s'>%s</option>", $nullValue, $label);
        }
        foreach ($options as $key => $option) {

            if (!is_array($selectedValue)) {
                $select .= sprintf("<option value ='%s' %s >%s</option>", $key, ($selectedValue === $key ? 'selected = "selected"' : ''), $option);
            } else {
                $select .= sprintf("<option value ='%s' %s >%s</option>", $key, (in_array($key, $selectedValue) ? 'selected = "selected"' : ''), $option);

            }
        }
        $select .= "</select>";
        echo $select;
    }

    /**
     * @param array $options A list of options. Key is used as value attribute, value is used as label
     * @param string $name
     * @param mixed $selectedValue
     * @param string $label Default (null) value label
     * @param array $attributes A list of attributes. Key is used as attribute name, value is used as attribute value
     * @param string $nullValue
     * @param string $class
     * @return string
     */
    public static function Select($options, $name, $selectedValue = null, $label = null, $attributes = [], $nullValue = "", $class = "form-control")
    {
        if (is_null($label)) {
            $label = "Select";
        }
        if (is_null($name)) {
            $name = "";
        }
        $select = sprintf("<select name='%s' class='%s' ", $name, $class);
        if (count($attributes) > 0) {
            foreach ($attributes as $key => $attribute) {
                $select .= sprintf('%s = "%s" ', $key, $attribute);
            }
        }
        $select .= ">";
        $select .= sprintf("<option value='%s'>%s</option>", $nullValue, $label);
        foreach ($options as $key => $option) {
            $select .= sprintf("<option value ='%s' %s >%s</option>", $key, ($selectedValue === $key ? 'selected = "selected"' : ''), $option);
        }
        $select .= "</select>";
        echo $select;
    }

    public static function Radio($options, $name, $selectedValue = null, $attributes = [])
    {
        if (is_null($name)) {
            $name = "";
        }

        $attrStrings = [];
        if (count($attributes) > 0) {
            foreach ($attributes as $key => $attribute) {
                $attrStrings[] = sprintf('%s = "%s"', $key, $attribute);
            }
        }

        $inputs = [];
        foreach ($options as $key => $option) {
            $inputId = sprintf("radio-input-%s-%s", $name, $key);
            $inputs[] = sprintf("<div class='radio'><label for='%s'><input type='radio' name='%s' %s id='%s' value='%s' %s /> %s</label></div>", $inputId, $name, ($selectedValue === $key ? 'checked' : ''), $inputId, $key, implode(" ", $attrStrings), $option);
        }

        echo implode("\n", $inputs);
    }

    public static function Checkbox($name, $checked, $value = 1)
    {
        echo  sprintf('<label class="switch switch-success"><input type="checkbox" name="%s" value="%s" %s/><span></span></label>', $name, $value, $checked ? "checked = ''" : "");
    }

}