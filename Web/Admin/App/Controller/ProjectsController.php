<?php

use Business\ApiControllers\ProjectsApiController;
use Business\Models\ProjectModel;

class ProjectsController extends MVCController {

	public function GetProjects() {
		$model = new ProjectsViewModel();
		$model->Projects = ProjectsApiController::GetProjects();

		$this->RenderView("Projects/Projects", ["model" => $model]);
	}

	public function GetAddProject() {
		$model = new ProjectViewModel();

		$this->RenderView("Projects/AddProject", ["model" => $model]);
	}


	public function PostAddProject($title, $subtitle, $description, $url, $image = null) {

		$model = new ProjectModel();
		$model->Title = $title;
		$model->Subtitle = $subtitle;
		$model->Description = $description;
		$model->Url = $url;


		if ($image['name'] !== "") {
			$newName = self::_generatePictureName($image['name'], $title);
			move_uploaded_file($image['tmp_name'], self::_generatePictureFullPath($newName));
			$model->Image = $newName;
		}

		ProjectsApiController::InsertProject($model);


		Router::Redirect("projects");
	}

	public function GetEditProject($projectId) {
		$model = new ProjectViewModel();

		$model->Project = ProjectsApiController::GetProject($projectId);

		$this->RenderView("Projects/EditProject", ["model" => $model]);
	}

	public function PostEditProject($projectId,$title, $subtitle, $description, $url, $image = null) {
		$model = ProjectsApiController::GetProject($projectId);
		$model->Title = $title;
		$model->Subtitle = $subtitle;
		$model->Description = $description;
		$model->Url = $url;


		if ($image['name'] !== "") {
			$newName = self::_generatePictureName($image['name'], $title);
			move_uploaded_file($image['tmp_name'], self::_generatePictureFullPath($newName));
			$model->Image = $newName;
		}

		ProjectsApiController::UpdateProject($model);


		Router::Redirect("projects");
	}


	public function GetDeleteProject($projectId) {

		ProjectsApiController::DeleteProject($projectId);
		Router::Redirect("projects");
	}


	private static function _generatePictureFullPath($pictureName) {
		return sprintf("%sMedia/Projects/%s", CDN_PATH, $pictureName);
	}

	private static function _generatePictureName($logo, $name) {
		return CommonHelper::StringToFilename(sprintf("project-%s-%s.%s", $name, CommonHelper::GenerateRandomString(), CommonHelper::GetExtension($logo)));
	}
}