<?php

/**
 * Class RoutesConfig
 */
class RoutesConfig {

	/**
	 * @return RouteDTO[]
	 */
	public static function GetRoutes() {
		$routes = array(
			"example" => new RouteDTO("example/{Id:integer}/{Slug:string}", "Home", "Example"),

			// Home
			"dashboard" => new RouteDTO("dashboard", "Home", "Index"),

			//Users
			"login" => new RouteDTO("login", "Security", "Login"),
			"logout" => new RouteDTO("logout", "Security", "Logout"),
			"users" => new RouteDTO("users", "Users", "Users"),
			"add-user" => new RouteDTO("add-user", "Users", "AddUser"),
			"edit-user" => new RouteDTO("edit-user/{userId:integer}", "Users", "EditUser"),
			"delete-user" => new RouteDTO("delete-user/{userId:integer}", "Users", "DeleteUser"),


			//Projects

			"projects" => new RouteDTO("projects", "Projects", "Projects"),
			"add-project" => new RouteDTO("add-project", "Projects", "AddProject"),
			"edit-project" => new RouteDTO("edit-project/{projectId:integer}", "Projects", "EditProject"),
			"delete-project" => new RouteDTO("delete-project/{projectId:integer}", "Projects", "DeleteProject"),


			// Maintenance
			"maintenance" => new RouteDTO("maintenance", "Maintenance", "Index"),
		);


		return $routes;
	}

}