<?php

use Business\Models\ProjectModel;
use Business\Models\UserModel;

/**
 * Class ProjectsViewModel
 * @property ProjectModel[] $Projects
 */
class ProjectsViewModel {

	public $Projects;

}