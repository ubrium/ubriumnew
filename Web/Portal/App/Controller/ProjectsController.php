<?php

use Business\ApiControllers\ProjectsApiController;
use Business\Models\ProjectModel;

class ProjectsController extends MVCController {

	public function GetPortfolio($projectId) {

		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Content-Type');

		$model = new ProjectViewModel();
		$model->Project = ProjectsApiController::GetProject($projectId);


		if ($model->Project->Image == null || $model->Project->Image == '') {
			$model->Project->Image = "http://ubriumnew.test/CDN/Media/DefaultImages/portfolio-default.jpg";
		} else {
			$model->Project->Image = "http://ubriumnew.test/CDN/Media/Projects/" . $model->Project->Image;
		}

		echo json_encode($model);

	}
}