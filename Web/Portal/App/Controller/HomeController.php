<?php

use Business\ApiControllers\ProjectsApiController;
use Business\ApiControllers\UsersApiController;
use Business\Models\ProjectModel;

class HomeController extends MVCController {

	public function GetIndex() {
		Router::Redirect("home");
	}

	public function GetHome() {

		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Content-Type');

		foreach (ProjectsApiController::GetProjects() as $pro) {

			$proBasic = new ProjectModel();

			$proBasic->ProjectId = $pro->ProjectId;
			$proBasic->Title = $pro->Title;
			$proBasic->Subtitle = $pro->Subtitle;
			$proBasic->Description = $pro->Description;

			if ($pro->Image == null || $pro->Image == '') {
				$pro->Image = "http://ubriumnew.test/CDN/Media/DefaultImages/portfolio-default.jpg";
			} else {
				$pro->Image = "http://ubriumnew.test/CDN/Media/Projects/" . $pro->Image;
			}

			$proBasic->Image = $pro->Image;

			$projects[] = $proBasic;

		}
		echo json_encode($projects);
	}

}