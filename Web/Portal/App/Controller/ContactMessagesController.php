<?php

use Business\ApiControllers\ContactMessagesApiController;
use Business\Models\ContactMessageModel;

class ContactMessagesController extends MVCController {

	public function GetContactMessage() {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Content-Type');

		$model = ContactMessagesApiController::GetContactMessages();

		echo json_encode($model);
	}

	public function PostContactMessage() {

		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: Content-Type');
		$postdata = file_get_contents("php://input");
		$data = json_decode($postdata, true);

		$model = new ContactMessageModel();

		$model->FirstName = $data['firstName'];
		$model->LastName = $data['lastName'];
		$model->Email = $data['email'];
		$model->Message = $data['message'];

		ContactMessagesApiController::InsertMessage($model);
	}

}