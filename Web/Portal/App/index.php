<?php
use Business\Enums\LocalesEnum;

try {
	include('../../../GlobalConfig.php');
	include('../../../GlobalAutoload.php');

	include('Components/Autoload.php');
	require_once('../../../Business/Lib/Gettext/gettext.inc');

	if (Config::debugMode === false) {
		error_reporting(0);
		ini_set('display_errors', 0);
	} else {
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
	}

	Session::Start();

	/*    $locale = LocalesEnum::GetConstant(Router::GetLanguage());
		// gettext setup
		T_setlocale(LC_MESSAGES, $locale);
		// Set the text domain as 'messages'
		$domain = 'messages';
		T_bindtextdomain($domain, LOCALE_DIR);
		T_bind_textdomain_codeset($domain, "UTF-8");
		T_textdomain($domain);*/

	$router = new Router();
	if (Config::maintenanceMode && "Maintenance" != $router->Controller) {
		Router::Redirect("maintenance");
	}

	Security::CheckLogin();

	$router->RenderPage();
} catch (MVCException $e) {
	$e->DisplayError();
} catch (Exception $e) {
	var_dump($e);
	die();
}
