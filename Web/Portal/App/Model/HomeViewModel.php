<?php

use Business\Models\UserModel;
use Business\Models\ProjectModel;

/**
 * Class HomeViewModel
 * @property UserModel[] $Users
 * @property ProjectModel[] $Projects
 */
class HomeViewModel {

	public $Users;
	public $Projects;

}