<?php

class Config
{
	const baseurl = PORTAL_URL;
	const maintenanceMode = MAINTENANCE_MODE_PORTAL;
	const debugMode = DEBUG_MODE_PORTAL;
	const useCache = USE_CACHE_PORTAL;
}