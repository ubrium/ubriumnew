/**
 * Created by mihaj on 10/22/2016.
 */
$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();

    console.log(scroll);

    if (scroll > 500) {
        $('.one-nav').addClass('scrolled');
    } else {
        $('.one-nav').removeClass('scrolled');
    }
});



$(document).ready(function () {

    /* waypoints */

    $('.iniAnim1')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('loadAnim1')
            }
        }, {
            offset: '90%'
        });

    $('.iniAnim2')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('loadAnim2')
            }
        }, {
            offset: '90%'
        });

    $('.iniAnim3')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('loadAnim3')
            }
        }, {
            offset: '90%'
        });

    $('.iniAnim4')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('loadAnim4')
            }
        }, {
            offset: '90%'
        });

    $('.iniAnim5')
        .waypoint(function (direction) {
            if (direction === 'down') {
                $(this.element).addClass('loadAnim5')
            }
        }, {
            offset: '90%'
        });

    /* slick slider */

    $('.portfolio-slider').slick({
        infinite: true,
        dots: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    $(function () {
        $('a[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
});