<?php

namespace Data\DataManagers;

use Data\Repositories\ProjectsRepository;

class ProjectsDataManager {

	public static function GetProjects() {
		return ProjectsRepository::Get();
	}


	public static function GetProject($projectId) {
		return ProjectsRepository::GetOne(["ProjectId" => $projectId]);
	}

	public static function InsertProject($model) {
		return ProjectsRepository::Insert($model);
	}

	public static function UpdateProject($model) {
		return ProjectsRepository::Update($model);
	}

	public static function DeleteProject($projectId) {
		return ProjectsRepository::Delete($projectId);
	}

	public static function CountProjects() {
		return ProjectsRepository::Count();
	}


}