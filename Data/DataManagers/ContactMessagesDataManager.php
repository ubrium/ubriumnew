<?php


namespace Data\DataManagers;

use Data\Repositories\ContactMessagesRepository;

class ContactMessagesDataManager {

	public static function GetContactMessages() {
		return ContactMessagesRepository::Get();
	}

	public static function InsertMessage($model) {
		return ContactMessagesRepository::Insert($model);
	}
}