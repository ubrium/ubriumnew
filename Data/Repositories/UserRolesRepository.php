<?php


namespace Data\Repositories;


use Business\Models\UserRoleModel;

/**
 * Class UserRolesRepository
 * @package Data\Repositories
 * @method static UserRoleModel[] Get
 * @method static UserRoleModel GetOne
 */
class UserRolesRepository extends BaseRepository {

}