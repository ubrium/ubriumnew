<?php

namespace Data\Repositories;
use Business\Models\ProjectModel;

/**
 * Class UsersRepository
 * @package Data\Repositories
 * @method static ProjectModel[] Get
 * @method static ProjectModel GetOne
 */
class ProjectsRepository extends BaseRepository {

}