<?php

namespace Data\Database\Protocol;


class ConnectionDetails {

    public $DatabaseName;
    public $Username;
    public $Password;
    public $Host;
    public $Port;

    public function __construct($dbName, $username = 'root', $password = '', $host = '127.0.0.1', $port = null) {
        $this->DatabaseName = $dbName;
        $this->Username = $username;
        $this->Password = $password;
        $this->Host = $host;
        $this->Port = $port;
    }

}