/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : ubriumnew

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-03-12 13:34:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `access_token_types`
-- ----------------------------
DROP TABLE IF EXISTS `access_token_types`;
CREATE TABLE `access_token_types` (
  `AccessTokenTypeId` int(10) NOT NULL,
  `Caption` varchar(50) NOT NULL,
  PRIMARY KEY (`AccessTokenTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of access_token_types
-- ----------------------------
INSERT INTO `access_token_types` VALUES ('1', 'Admin');
INSERT INTO `access_token_types` VALUES ('2', 'Portal');

-- ----------------------------
-- Table structure for `confirmation_links`
-- ----------------------------
DROP TABLE IF EXISTS `confirmation_links`;
CREATE TABLE `confirmation_links` (
  `ConfirmationLinkId` int(10) NOT NULL AUTO_INCREMENT,
  `UserId` int(10) NOT NULL,
  `ExpirationDate` datetime NOT NULL,
  `ConfirmationLink` varchar(250) NOT NULL,
  PRIMARY KEY (`ConfirmationLinkId`),
  KEY `confirmation_links_ibfk_1` (`UserId`) USING BTREE,
  CONSTRAINT `confirmation_links_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of confirmation_links
-- ----------------------------

-- ----------------------------
-- Table structure for `password_reset_links`
-- ----------------------------
DROP TABLE IF EXISTS `password_reset_links`;
CREATE TABLE `password_reset_links` (
  `PasswordResetLinkId` int(10) NOT NULL AUTO_INCREMENT,
  `UserId` int(10) NOT NULL,
  `ExpirationDate` datetime NOT NULL,
  `ResetLink` varchar(250) NOT NULL,
  PRIMARY KEY (`PasswordResetLinkId`),
  KEY `confirmation_links_ibfk_1` (`UserId`) USING BTREE,
  CONSTRAINT `password_reset_links_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of password_reset_links
-- ----------------------------

-- ----------------------------
-- Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `PermissionId` int(11) NOT NULL AUTO_INCREMENT,
  `Caption` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`PermissionId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'Dashboard', null);
INSERT INTO `permissions` VALUES ('2', 'ViewUsers', null);
INSERT INTO `permissions` VALUES ('3', 'AddUsers', null);
INSERT INTO `permissions` VALUES ('4', 'EditUsers', null);
INSERT INTO `permissions` VALUES ('5', 'DeleteUsers', null);
INSERT INTO `permissions` VALUES ('20', 'Profile', null);

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `RoleId` int(11) NOT NULL AUTO_INCREMENT,
  `Protected` tinyint(4) NOT NULL,
  `Active` tinyint(4) NOT NULL,
  `Caption` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`RoleId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', '1', '1', 'Administrator');
INSERT INTO `roles` VALUES ('2', '1', '1', 'User');
INSERT INTO `roles` VALUES ('3', '1', '1', 'Visitor');

-- ----------------------------
-- Table structure for `role_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE `role_permissions` (
  `RolePermissionId` int(11) NOT NULL AUTO_INCREMENT,
  `RoleId` int(11) NOT NULL,
  `PermissionId` int(11) NOT NULL,
  `Protected` smallint(6) NOT NULL,
  PRIMARY KEY (`RolePermissionId`),
  KEY `FK_RolePermissions_Permissions` (`PermissionId`) USING BTREE,
  KEY `FK_RolePermissions_Roles` (`RoleId`) USING BTREE,
  CONSTRAINT `role_permissions_ibfk_1` FOREIGN KEY (`PermissionId`) REFERENCES `permissions` (`PermissionId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_permissions_ibfk_2` FOREIGN KEY (`RoleId`) REFERENCES `roles` (`RoleId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_permissions
-- ----------------------------
INSERT INTO `role_permissions` VALUES ('1', '1', '1', '0');
INSERT INTO `role_permissions` VALUES ('2', '1', '20', '0');
INSERT INTO `role_permissions` VALUES ('3', '2', '20', '0');
INSERT INTO `role_permissions` VALUES ('4', '1', '2', '0');
INSERT INTO `role_permissions` VALUES ('5', '1', '4', '0');
INSERT INTO `role_permissions` VALUES ('6', '1', '5', '0');
INSERT INTO `role_permissions` VALUES ('7', '1', '3', '0');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `UserId` int(10) NOT NULL AUTO_INCREMENT,
  `Email` varchar(250) NOT NULL,
  `Password` varchar(250) DEFAULT NULL,
  `Name` varchar(250) NOT NULL,
  `Username` varchar(255) DEFAULT NULL,
  `Image` varchar(250) DEFAULT NULL,
  `RegistrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ConfirmRegistration` tinyint(1) NOT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `UniqueEmail` (`Email`) USING BTREE,
  UNIQUE KEY `UnigueUsername` (`Username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'john@doe.com', '$2a$10$9dRxNcvXVsK.Etoyx9yOoO2/GdYWJWzQ4eysCy3LHMbyPVRNeDRoy', 'John Doe', 'johndoe', '', '2017-02-08 16:42:33', '1', '1');

-- ----------------------------
-- Table structure for `user_access_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `user_access_tokens`;
CREATE TABLE `user_access_tokens` (
  `UserAccessTokenId` int(11) NOT NULL AUTO_INCREMENT,
  `Token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `UserId` int(11) NOT NULL,
  `StartDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `EndDate` datetime DEFAULT NULL,
  `AccessTokenTypeId` int(10) NOT NULL,
  PRIMARY KEY (`UserAccessTokenId`),
  UNIQUE KEY `Unique_Token` (`Token`) USING BTREE,
  KEY `FK_UserAccessTokens_Users` (`UserId`) USING BTREE,
  KEY `TypeId` (`AccessTokenTypeId`) USING BTREE,
  CONSTRAINT `user_access_tokens_ibfk_1` FOREIGN KEY (`AccessTokenTypeId`) REFERENCES `access_token_types` (`AccessTokenTypeId`) ON UPDATE CASCADE,
  CONSTRAINT `user_access_tokens_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for `user_roles`
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `UserRoleId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `RoleId` int(11) NOT NULL,
  PRIMARY KEY (`UserRoleId`),
  KEY `FK_UserRoles_Users` (`UserId`) USING BTREE,
  KEY `FK_UserRoles_Roles` (`RoleId`) USING BTREE,
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`RoleId`) REFERENCES `roles` (`RoleId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES ('1', '1', '1');
